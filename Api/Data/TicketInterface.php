<?php
/**
 * Shine Software Helpdesk System
 * Copyright (C) 2020 Shine Software
 * 
 * This file is part of Mycompany/Helpdesk.
 * 
 * Mycompany/Helpdesk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mycompany\Helpdesk\Api\Data;

interface TicketInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const TICKET_ID = 'ticket_id';
    const SUBJECT = 'subject';

    /**
     * Get ticket_id
     * @return string|null
     */
    public function getTicketId();

    /**
     * Set ticket_id
     * @param string $ticketId
     * @return \Mycompany\Helpdesk\Api\Data\TicketInterface
     */
    public function setTicketId($ticketId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Mycompany\Helpdesk\Api\Data\TicketExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Mycompany\Helpdesk\Api\Data\TicketExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Mycompany\Helpdesk\Api\Data\TicketExtensionInterface $extensionAttributes
    );

    /**
     * Get subject
     * @return string|null
     */
    public function getSubject();

    /**
     * Set subject
     * @param string $subject
     * @return \Mycompany\Helpdesk\Api\Data\TicketInterface
     */
    public function setSubject($subject);
}
