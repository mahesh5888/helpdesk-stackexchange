<?php
/**
 * Shine Software Helpdesk System
 * Copyright (C) 2020 Shine Software
 * 
 * This file is part of Mycompany/Helpdesk.
 * 
 * Mycompany/Helpdesk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mycompany\Helpdesk\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface ThreadrelatedRepositoryInterface
 *
 * @package Mycompany\Helpdesk\Api
 */
interface ThreadrelatedRepositoryInterface
{

    /**
     * Save Threadrelated
     * @param \Mycompany\Helpdesk\Api\Data\ThreadrelatedInterface $threadrelated
     * @return \Mycompany\Helpdesk\Api\Data\ThreadrelatedInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Mycompany\Helpdesk\Api\Data\ThreadrelatedInterface $threadrelated
    );

    /**
     * Retrieve Threadrelated
     * @param string $threadrelatedId
     * @return \Mycompany\Helpdesk\Api\Data\ThreadrelatedInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($threadrelatedId);

    /**
     * Retrieve Threadrelated matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Mycompany\Helpdesk\Api\Data\ThreadrelatedSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Threadrelated
     * @param \Mycompany\Helpdesk\Api\Data\ThreadrelatedInterface $threadrelated
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Mycompany\Helpdesk\Api\Data\ThreadrelatedInterface $threadrelated
    );

    /**
     * Delete Threadrelated by ID
     * @param string $threadrelatedId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($threadrelatedId);
}

