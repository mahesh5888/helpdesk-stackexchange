<?php
/**
 * My Company Helpdesk System
 * Copyright (C) 2020 My Company
 * 
 * This file is part of Mycompany/Helpdesk.
 * 
 * Mycompany/Helpdesk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mycompany\Helpdesk\Controller\Adminhtml\Ticket;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\View\Result\PageFactory;
use Mycompany\Helpdesk\Api\TicketRepositoryInterface;

class Edit extends \Mycompany\Helpdesk\Controller\Adminhtml\Ticket
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var SessionManagerInterface
     */
    protected $coreSession;

    /**
     * @var TicketRepositoryInterface
     */
    protected $ticket;

    /**
     * Edit constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param SessionManagerInterface $coreSession
     * @param PageFactory $resultPageFactory
     * @param TicketRepositoryInterface $ticket
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        SessionManagerInterface $coreSession,
        PageFactory $resultPageFactory,
        TicketRepositoryInterface $ticket
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->ticket = $ticket;
        $this->coreSession = $coreSession;
        parent::__construct($context, $coreRegistry);
    }

    /**
     * Edit action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('ticket_id');

        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        // 2. Initial checking
        if ($id) {
            $ticket = $this->ticket->get($id);
            $this->coreSession->setTicketId($id);

            if (!$ticket->getTicketId()) {
                $this->messageManager->addErrorMessage(__('This Ticket no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            $this->_coreRegistry->register('mycompany_helpdesk_ticket', $ticket);

            // 3. Build edit form
            /** @var Page $resultPage */
            $resultPage = $this->resultPageFactory->create();
            $this->initPage($resultPage)->addBreadcrumb(
                $id ? __('Edit Ticket') : __('New Ticket'),
                $id ? __('Edit Ticket') : __('New Ticket')
            );
            $resultPage->getConfig()->getTitle()->prepend(__('Tickets'));
            $resultPage->getConfig()->getTitle()->prepend($ticket->getTicketId() ? __('Ticket #%1 - %2', $ticket->getTicketId(), $ticket->getSubject()) : __('New Ticket'));
            return $resultPage;

        }
        return $resultRedirect->setPath('/helpdesk/ticket/index');
    }
}
