<?php
/**
 * My Company Helpdesk System
 * Copyright (C) 2020 My Company
 *
 * This file is part of Mycompany/Helpdesk.
 *
 * Mycompany/Helpdesk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mycompany\Helpdesk\Controller\Adminhtml\Ticket;

use Magento\Framework\Exception\LocalizedException;

class Update extends \Magento\Backend\App\Action
{

    /**
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * @var \Mycompany\Helpdesk\Api\TicketRepositoryInterface
     */
    protected $ticket;

    /**
     * @var \Mycompany\Helpdesk\Model\Thread
     */
    protected $thread;

    /**
     * @var \Mycompany\Helpdesk\Model\ThreadRepository
     */
    protected $threadRepository;

    /**
     * @var \Mycompany\Helpdesk\Model\Attachments
     */
    protected $attachments;

    /**
     * Update constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Mycompany\Helpdesk\Api\TicketRepositoryInterface $ticket
     * @param \Mycompany\Helpdesk\Model\Thread $thread
     * @param \Mycompany\Helpdesk\Model\ThreadRepository $threadRepository
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Mycompany\Helpdesk\Api\TicketRepositoryInterface $ticket,
        \Mycompany\Helpdesk\Model\Thread $thread,
        \Mycompany\Helpdesk\Model\Attachments $attachments,
        \Mycompany\Helpdesk\Model\ThreadRepository $threadRepository,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->date = $date;
        $this->thread = $thread;
        $this->threadRepository = $threadRepository;
        $this->ticket = $ticket;
        $this->attachments = $attachments;

        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        $attachment = null;

        // prepare the event dispacher
        $this->_eventManager->dispatch('mycompany_helpdesk_thread_update_before', ['data' => $data]);

        if ($data) {
            $MainParams = $this->getRequest()->getParam('general');
            $id = $MainParams['ticket_id'];

            /** @var \Mycompany\Helpdesk\Model\Ticket $ticket */
            $ticket = $this->ticket->get($id);

            if (!$ticket->getTicketId() && $id) {
                $this->messageManager->addErrorMessage(__('This Ticket no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            // check the overwritten values.
            $replyDate = empty($data['options']['reply_date']) ? $this->date->gmtDate() : $data['options']['reply_date'];

            try {
                // Create a thread reply
                if(!empty($data['reply']['message'])){

                    // prepare the event dispacher
                    $this->_eventManager->dispatch('mycompany_helpdesk_thread_update_before', ['data' => $data]);

                    // check if there is an attachment
                    if(!empty($data['summary']['attachment'][0])){
                        $attachment = $data['summary']['attachment'][0];
                    }

                    if(empty($data['reply']['parent_id'])){
                        $data['reply']['parent_id'] = null;
                    }

                    // save the thread
                    $thread = $this->thread->prepare($id, $data['reply']['message'], $replyDate, $attachment, $data['reply']['parent_id'], $data['furtherinfo']['generated_by'], $data['options']['status_id']);
                    $this->_eventManager->dispatch('mycompany_helpdesk_thread_update_after', ['thread' => $thread, 'data' => $data]);

                }else{

                    // if there is not any reply message from the administration panel
                    // we have to get the first original thread
                    $firstThreadId = $this->threadRepository->getThreadIdByTicketId($data['general']['ticket_id'], 'ASC');
                    $thread = $this->threadRepository->get($firstThreadId);
                }

                // prepare the event dispacher
                $this->_eventManager->dispatch('mycompany_helpdesk_thread_update_complete', ['thread' => $thread, 'data' => $data]);

                $this->messageManager->addSuccessMessage(__('You saved the Ticket.'));
                $this->dataPersistor->clear('mycompany_helpdesk_ticket');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['ticket_id' => $ticket->getTicketId()]);
                }
                return $resultRedirect->setPath('*/*/');

            } catch (\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Ticket.'));
            }
        }

        $this->dataPersistor->set('mycompany_helpdesk_ticket', $data);
        return $resultRedirect->setPath('*/*/edit', ['ticket_id' => $this->getRequest()->getParam('ticket_id')]);
    }
}
