<?php
/**
 * Shine Software Helpdesk System
 * Copyright (C) 2020 Shine Software
 * 
 * This file is part of Mycompany/Helpdesk.
 * 
 * Mycompany/Helpdesk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mycompany\Helpdesk\Model\Data;

use Mycompany\Helpdesk\Api\Data\ThreadInterface;

class Thread extends \Magento\Framework\Api\AbstractExtensibleObject implements ThreadInterface
{

    /**
     * Get thread_id
     * @return string|null
     */
    public function getThreadId()
    {
        return $this->_get(self::THREAD_ID);
    }

    /**
     * Set thread_id
     * @param string $threadId
     * @return \Mycompany\Helpdesk\Api\Data\ThreadInterface
     */
    public function setThreadId($threadId)
    {
        return $this->setData(self::THREAD_ID, $threadId);
    }

    /**
     * Get ticket_id
     * @return string|null
     */
    public function getTicketId()
    {
        return $this->_get(self::TICKET_ID);
    }

    /**
     * Set ticket_id
     * @param string $ticketId
     * @return \Mycompany\Helpdesk\Api\Data\ThreadInterface
     */
    public function setTicketId($ticketId)
    {
        return $this->setData(self::TICKET_ID, $ticketId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Mycompany\Helpdesk\Api\Data\ThreadExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Mycompany\Helpdesk\Api\Data\ThreadExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Mycompany\Helpdesk\Api\Data\ThreadExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

}
