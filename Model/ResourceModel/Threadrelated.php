<?php
/**
 * Shine Software Helpdesk System
 * Copyright (C) 2020 Shine Software
 * 
 * This file is part of Mycompany/Helpdesk.
 * 
 * Mycompany/Helpdesk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mycompany\Helpdesk\Model\ResourceModel;

/**
 * Class Threadrelated
 *
 * @package Mycompany\Helpdesk\Model\ResourceModel
 */
class Threadrelated extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('mycompany_helpdesk_threadrelated', 'threadrelated_id');
    }


    /**
     * @param string $field
     * @param mixed $value
     * @param \Magento\Framework\Model\AbstractModel $object
     * @return \Magento\Framework\DB\Select
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _getLoadSelect($field, $value, $object){

        $select = $this->getConnection()
            ->select()
            ->from($this->getMainTable())
            ->where($this->getMainTable() . '.' . $field . '=?', $value)
            ->joinLeft(
                ['ticket' => $this->getTable('mycompany_helpdesk_ticket')],
                $this->getTable('mycompany_helpdesk_threadrelated').'.ticket_id = ticket.ticket_id',
                ['subject'] // '*' define that you want all column of 2nd table.  you can define as ['column1','column2']
            )->joinLeft(
                ['thread' => $this->getTable('mycompany_helpdesk_thread')],
                $this->getTable('mycompany_helpdesk_threadrelated').'.thread_id = thread.thread_id',
                ['*'] // '*' define that you want all column of 2nd table.  you can define as ['column1','column2']
            );

        return $select;
    }
}

