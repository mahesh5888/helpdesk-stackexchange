<?php
/**
 * Shine Software Helpdesk System
 * Copyright (C) 2020 Shine Software
 * 
 * This file is part of Mycompany/Helpdesk.
 * 
 * Mycompany/Helpdesk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mycompany\Helpdesk\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\DataObjectHelper;
use Mycompany\Helpdesk\Model\ResourceModel\Threadrelated\CollectionFactory as ThreadrelatedCollectionFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Mycompany\Helpdesk\Api\Data\ThreadrelatedInterfaceFactory;
use Mycompany\Helpdesk\Api\ThreadrelatedRepositoryInterface;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Mycompany\Helpdesk\Api\Data\ThreadrelatedSearchResultsInterfaceFactory;
use Mycompany\Helpdesk\Model\ResourceModel\Threadrelated as ResourceThreadrelated;

/**
 * Class ThreadrelatedRepository
 *
 * @package Mycompany\Helpdesk\Model
 */
class ThreadrelatedRepository implements ThreadrelatedRepositoryInterface
{

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    protected $dataThreadrelatedFactory;

    private $collectionProcessor;

    protected $threadrelatedCollectionFactory;

    protected $resource;

    private $storeManager;

    protected $extensibleDataObjectConverter;
    protected $threadrelatedFactory;


    /**
     * @param ResourceThreadrelated $resource
     * @param ThreadrelatedFactory $threadrelatedFactory
     * @param ThreadrelatedInterfaceFactory $dataThreadrelatedFactory
     * @param ThreadrelatedCollectionFactory $threadrelatedCollectionFactory
     * @param ThreadrelatedSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceThreadrelated $resource,
        ThreadrelatedFactory $threadrelatedFactory,
        ThreadrelatedInterfaceFactory $dataThreadrelatedFactory,
        ThreadrelatedCollectionFactory $threadrelatedCollectionFactory,
        ThreadrelatedSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->threadrelatedFactory = $threadrelatedFactory;
        $this->threadrelatedCollectionFactory = $threadrelatedCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataThreadrelatedFactory = $dataThreadrelatedFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Mycompany\Helpdesk\Api\Data\ThreadrelatedInterface $threadrelated
    ) {
        /* if (empty($threadrelated->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $threadrelated->setStoreId($storeId);
        } */
        
        $threadrelatedData = $this->extensibleDataObjectConverter->toNestedArray(
            $threadrelated,
            [],
            \Mycompany\Helpdesk\Api\Data\ThreadrelatedInterface::class
        );
        
        $threadrelatedModel = $this->threadrelatedFactory->create()->setData($threadrelatedData);
        
        try {
            $this->resource->save($threadrelatedModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the threadrelated: %1',
                $exception->getMessage()
            ));
        }
        return $threadrelatedModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($threadrelatedId)
    {
        $threadrelated = $this->threadrelatedFactory->create();
        $this->resource->load($threadrelated, $threadrelatedId);
        if (!$threadrelated->getId()) {
            throw new NoSuchEntityException(__('Threadrelated with id "%1" does not exist.', $threadrelatedId));
        }
        return $threadrelated->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->threadrelatedCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Mycompany\Helpdesk\Api\Data\ThreadrelatedInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Mycompany\Helpdesk\Api\Data\ThreadrelatedInterface $threadrelated
    ) {
        try {
            $threadrelatedModel = $this->threadrelatedFactory->create();
            $this->resource->load($threadrelatedModel, $threadrelated->getThreadrelatedId());
            $this->resource->delete($threadrelatedModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Threadrelated: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($threadrelatedId)
    {
        return $this->delete($this->get($threadrelatedId));
    }
}

