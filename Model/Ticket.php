<?php
/**
 * Shine Software Helpdesk System
 * Copyright (C) 2020 Shine Software
 * 
 * This file is part of Mycompany/Helpdesk.
 * 
 * Mycompany/Helpdesk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mycompany\Helpdesk\Model;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;
use Mycompany\Helpdesk\Api\Data\TicketInterface;
use Mycompany\Helpdesk\Api\Data\TicketInterfaceFactory;

class Ticket extends \Magento\Framework\Model\AbstractModel
{

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var TicketInterfaceFactory
     */
    protected $ticketFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customer;

    /**
     * @var string
     */
    protected $_eventPrefix = 'mycompany_helpdesk_ticket';

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * @var \Mycompany\Helpdesk\Api\TicketRepositoryInterface
     */
    protected $ticketRepository;

    /**
     * Ticket constructor.
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param TicketInterfaceFactory $ticketFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param CustomerRepositoryInterface $customer
     * @param ResourceModel\Ticket $resource
     * @param ResourceModel\Ticket\Collection $resourceCollection
     * @param \Mycompany\Helpdesk\Api\TicketRepositoryInterface $ticketRepository
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        TicketInterfaceFactory $ticketFactory,
        DataObjectHelper $dataObjectHelper,
        CustomerRepositoryInterface $customer,
        \Mycompany\Helpdesk\Model\ResourceModel\Ticket $resource,
        \Mycompany\Helpdesk\Model\ResourceModel\Ticket\Collection $resourceCollection,
        \Mycompany\Helpdesk\Api\TicketRepositoryInterface $ticketRepository,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        array $data = []
    ) {
        $this->ticketFactory = $ticketFactory;
        $this->ticketRepository = $ticketRepository;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->storeManager = $storeManager;
        $this->customer = $customer;

        $this->date = $date;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Get the ticket by Id
     *
     * @param $id
     * @return TicketInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id)
    {
        return $this->ticketRepository->get($id);
    }


    /**
     * Save the ticket information
     *
     * @param $data
     * @return TicketInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */

    /**
     * Retrieve ticket model with ticket data
     * @return TicketInterface
     */
    public function getDataModel()
    {
        $ticketData = $this->getData();

        $ticketDataObject = $this->ticketFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $ticketDataObject,
            $ticketData,
            TicketInterface::class
        );
        
        return $ticketDataObject;
    }


}
