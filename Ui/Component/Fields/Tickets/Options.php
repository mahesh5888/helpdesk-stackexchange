<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Mycompany\Helpdesk\Ui\Component\Fields\Tickets;

use Magento\Framework\Data\OptionSourceInterface;
use Mycompany\Helpdesk\Api\TicketRepositoryInterface;
use Mycompany\Helpdesk\Model\ResourceModel\Thread\Collection;
use Mycompany\Helpdesk\Model\ResourceModel\Threadrelated\CollectionFactory;
use Mycompany\Helpdesk\Model\ResourceModel\Ticket\CollectionFactory as TicketCollectionFactory;
use Mycompany\Helpdesk\Model\ResourceModel\Thread\CollectionFactory as ThreadCollectionFactory;
use Magento\Framework\App\RequestInterface;

/**
 * Options tree for "Categories" field
 */
class Options implements OptionSourceInterface
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Ticket\CollectionFactory
     */
    protected $ticketCollectionFactory;

    /**
     * @var ThreadCollectionFactory
     */
    protected $threadCollectionFactory;

    /**
     * @var TicketRepositoryInterface
     */
    protected $ticketRepository;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var array
     */
    protected $ticketlist;

    /**
     * @var CollectionFactory
     */
    protected $threadrelatedCollectionFactory;

    /**
     * Options constructor.
     * @param TicketCollectionFactory $ticketCollectionFactory
     * @param ThreadCollectionFactory $threadCollectionFactory
     * @param CollectionFactory $threadrelatedCollectionFactory
     * @param TicketRepositoryInterface $ticketRepository
     * @param RequestInterface $request
     */
    public function __construct(
        TicketCollectionFactory $ticketCollectionFactory,
        ThreadCollectionFactory $threadCollectionFactory,
        CollectionFactory $threadrelatedCollectionFactory,
        TicketRepositoryInterface $ticketRepository,
        RequestInterface $request
    ) {
        $this->ticketCollectionFactory = $ticketCollectionFactory;
        $this->threadCollectionFactory = $threadCollectionFactory;
        $this->threadrelatedCollectionFactory = $threadrelatedCollectionFactory;
        $this->ticketRepository = $ticketRepository;
        $this->request = $request;
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return $this->getTickets();
    }

    /**
     * Retrieve categories tree
     *
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getTickets()
    {
        if ($this->ticketlist === null) {
            $ticketId = $this->request->getParam('ticket_id');

            if(is_numeric($ticketId)){
                $ticket = $this->ticketRepository->get($ticketId);
                $customerId = $ticket->getCustomerId();
                $ticketById = array();

                /* @var $collection TicketCollectionFactory*/
                $tickets = $this->ticketCollectionFactory->create();
                // $tickets->addFieldToFilter('customer_id', ['eq' => $customerId]);

                // check the references of the ticket
                $ticketReferences = $this->getReferences($ticketId);
                $tickets->addFieldToFilter('ticket_id', ['nin' => $ticketReferences]);

                $ticketById[$customerId] = ['is_active' => null, 'value' => null, 'label' => null, 'optgroup' => []];

                foreach ($tickets as $ticket) {
                    $ticketById[$customerId]['optgroup'][] = ['is_active' => 1, 'value' => $ticket->getTicketId(), 'label' => $ticket->getTicketId() . " - " . $ticket->getSubject()];
                }

                $this->ticketlist = $ticketById[$customerId]['optgroup'];
            }
        }

        return $this->ticketlist;
    }

    /**
     * Get all the ticket references already attached to the selected ticket
     *
     * @param $ticketId
     * @return array
     */
    private function getReferences($ticketId){

        $tickets[] = $ticketId; // add itself to avoid a recursion of ticket

        $threadrelated = $this->threadrelatedCollectionFactory->create();
        $threadrelated->addFieldToFilter('ticket_id', ['eq' => $ticketId]);

        foreach ($threadrelated as $reference){
            $tickets[] = $reference->getReferenceId();
        }

        return $tickets;

    }
}
