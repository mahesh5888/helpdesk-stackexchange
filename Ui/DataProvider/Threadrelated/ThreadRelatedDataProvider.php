<?php

namespace Mycompany\Helpdesk\Ui\DataProvider\Threadrelated;

use Magento\Framework\App\Request\Http;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\View\Element\UiComponent\DataProvider\DataProvider;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\ReportingInterface;
use Magento\Framework\Api\Search\SearchCriteriaBuilder;
use Magento\Framework\App\RequestInterface;
use Mycompany\Helpdesk\Model\Ticket;

class ThreadRelatedDataProvider extends DataProvider
{
    /**
     * @var \Magento\Ui\DataProvider\AddFieldToCollectionInterface[]
     */
    protected $addFieldStrategies;

    /**
     * @var \Magento\Ui\DataProvider\AddFilterToCollectionInterface[]
     */
    protected $addFilterStrategies;

    /**
     * @var Http
     */
    protected $request;

    /**
     * @var Ticket
     */
    protected $ticket;

    protected $urlBuilder;
    protected $coreSession;

    /**
     * ThreadRelatedDataProvider constructor.
     * @param $name
     * @param $primaryFieldName
     * @param $requestFieldName
     * @param ReportingInterface $reporting
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param RequestInterface $request
     * @param FilterBuilder $filterBuilder
     * @param OrderRepositoryInterface $orderRepository
     * @param UrlInterface $urlBuilder
     * @param SessionManagerInterface $coreSession
     * @param Ticket $ticket
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        ReportingInterface $reporting,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        RequestInterface $request,
        FilterBuilder $filterBuilder,
        OrderRepositoryInterface $orderRepository,
        UrlInterface $urlBuilder,
        SessionManagerInterface $coreSession,
        Ticket $ticket,
        array $meta = [],
        array $data = []
    )
    {
        $this->urlBuilder = $urlBuilder;
        $this->request = $request;
        $this->ticket = $ticket;
        $this->data = $data;
        $this->coreSession = $coreSession;

        parent::__construct($name, $primaryFieldName, $requestFieldName, $reporting, $searchCriteriaBuilder, $request, $filterBuilder, $meta, $data);
    }

    /***
     * @return array
     */
    public function getData()
    {

        $collection = $this->getSearchResult();

        /** see: check Mycompany\Helpdesk\Controller\Adminhtml\Ticket\Edit **/
        if($this->coreSession->getTicketId()){
            $collection->addFieldToFilter('ticket_id', ['eq' => $this->coreSession->getTicketId()]);
        }

        return $this->searchResultToOutput($collection);

    }

}